from os import system
from time import sleep

#função para motrar menssagens
def show_message(first: str, second: str, style = "default"):
    if style == "default":
        show_animation(first)
        sleep(0.15)
        system("cls")
        show_animation(second)

#estilo padrão de animação
def show_animation(message):
    print("")
    print("   |")
    print("- {} -".format(message))
    print("   |")
    print("")
    sleep(0.1)
    system("cls")
    print("   |")
    print("   |")
    print("-  {}  -".format(message))
    print("   |")
    print("   |")
    sleep(0.1)
    system("cls")
    print("     |")
    print("     ")
    print("--   {}   --".format(message))
    print("     ")
    print("     |")
