from tqdm import tqdm
from time import sleep
from os import system
from animation import show_message

#animação de barra de carregamento
for i in tqdm(range(100)):
    sleep(0.1)
system('cls')

#obtenção de lados do triangulo
number1 = float(input("insert the first side: "))
number2 = float(input("insert the second side: "))
number3 = float(input("insert the third side: "))
system('cls')

#calculo de condição
condition = lambda number1, number2, number3: abs(number1 - number3) < number2 < number1 + number3

#mostra o resultado
if condition(number1,number2,number3) and condition(number3,number1,number2) and condition(number2,number3,number1):
    #print("form a triangule")
    show_message("result","form a triangule")
else:
    #print("don't form a triangule")
    show_message("result","don't form a triangule")
